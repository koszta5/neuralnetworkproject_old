'''
Created on Feb 13, 2012

@author: Pavel Kostelnik
'''

class DataSample(object):
    '''
    This object is data sample representation that is fed to Neural Network
    '''


    def __init__(self, day=0, month=0, year=0, price=0, audition=0, star=0):
        '''
        Object is fully initialized in contructor
        @param day: day of month
        @param month: month of year
        @param year: year -> not evaluated later but still kept in the sample object
        @param price: price of the movie
        @param audition: number of people who saw the movie screening
        '''
        self.day = day
        self.month = month
        self.year = year
        self.price = price
        self.audition = audition
        self.star = star
        