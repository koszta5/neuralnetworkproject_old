'''
Created on Feb 13, 2012

@author: Pavel Kostelnik
'''
import xlrd


from paralelSampleContructor import ParalelSampleConstructor
class DataSetProvider(object):
    '''
    This class is an excell file loader that gets the values necessary for computation
    '''


    def __init__(self, path_to_file):
        '''
        Constructor
        @param path_to_file: path to file where the excell is loaded
        '''
        self.samples = []
        self.loadFile(path_to_file)
        self.loadData()
        
    
    def loadFile(self, file_path):
        """
        This function loads excel file and gets the worksheet we work with
        @param file_path: path to excell file with data
        """
        self.workbook = xlrd.open_workbook(file_path)
        self.sheet = self.workbook.sheet_by_index(0)
    
    def addDataSample(self,DataSampleInstance):
        self.samples.append(DataSampleInstance)
        
    def loadData(self):
        """
        This funciton reads the excell line by line -> gets the important cells and sends their content to ParalelSampleContructor (class that handles theraded sample creation)
        """
        for rownum in range(self.sheet.nrows):
            date = self.sheet.row_values(rownum)[0]
            time = self.sheet.row_values(rownum)[1]
            audition = self.sheet.row_values(rownum)[2]
            price = self.sheet.row_values(rownum)[6]
            star = self.sheet.row_values(rownum)[5]
            instance = ParalelSampleConstructor(dataSetProvider = self, date = date, audition = audition, price = price, star=star)
            instance.start()
            
                
    
        
    def debug(self, what, where):
        """
        Simple debug logger function that prints debug code
        @param what: what to print
        @param where: where is the what found
        """
        print "in file " + str(self.workbook) + "there is " + str(what) + " on position " + str(where) + "\n" 
        