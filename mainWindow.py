# -*- coding: utf-8 -*-

'''
Created on Feb 14, 2012

@author: Pavel Kostelnik
'''
import sys,os,operator
from PyQt4 import QtGui, QtCore
from window import Ui_NeuralPredictor
import re
from window2 import Ui_NeuralPredictor2
from dataSetProvider import DataSetProvider
from paralelTableFiller import ParalelTableFiller
from network import Network

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class MainWindow(QtGui.QMainWindow):
    '''
    This is the main window of the app - it handles the main GUI thread
    '''


    def __init__(self,app):
        '''
        create the first window from QtDesigner designed file and connect the corresponding signals. Show gui and make app exitable
        @param app: the main class of the application that holds references to other background instances
        '''
        
        self.createQapp()
        QtGui.QMainWindow.__init__(self)
        self.ui = Ui_NeuralPredictor()
        self.ui.setupUi(self)
        self.connectSingalsWindow1()
        self.main = app
        
        self.show()
        self.run()
        
    def createQapp(self):
        """
        Qt necessary method
        """
        self.app = QtGui.QApplication(sys.argv)
        
       
    def run(self):
        """
        Make app exitable
        """
        sys.exit(self.app.exec_())
        
    def connectSingalsWindow1(self):
        """
        Connects singals to first wizard window
        """
        self.setWindowTitle("Neural network project")
        self.ui.statusbar.setSizeGripEnabled(False)
        self.ui.textPath.textChanged.connect(self.checkXlsInText)
        self.ui.fileChooseBtn.clicked.connect(self.selectFile)
        self.ui.nextBtn.clicked.connect(self.goForwardToSamples)
        
        
    def calldebug(self):
        """
        simple debug function just as a convinience thing
        """
        print "ahoj"
    
    
    def checkXlsInText(self):
        """
        This function checks whether the given location ends with .xls => we picked xls file
        """
        text = self.ui.textPath.text()
        if (re.match(r'^.*\.xls$', text)):
            return True
            
    def selectFile(self):
        """
        Open file dialog and select the file to be used as the source of samples. This changes the progress bar value
        """
        
        self.main.sourceFile = QtGui.QFileDialog.getOpenFileName(self, 'Open source File', os.path.expanduser("~"))
        self.ui.textPath.setText(self.main.sourceFile)
        self.ui.progressBar.setValue(5)
        if (self.checkXlsInText()):
            self.ui.nextBtn.setEnabled(True)
            self.ui.progressBar.setValue(99)
            
    def goForwardToSamples(self):
        """
        Click next to get from window1 to window 2. Also start a non-gui thread that will create structural data for tableView
        """
        self.main.dataSet = DataSetProvider(self.main.sourceFile)
        self.ui = Ui_NeuralPredictor2()
        self.ui.setupUi(self)
        self.show()
        self.paralelFiller = ParalelTableFiller(self)
        self.connectSignalsWindow2()
        self.paralelFiller.start()
        
    def connectSignalsWindow2(self):
        """
        Connect signals to window2 - samples. Allow going back. Also connect singals emited by the non-gui thread preparing the tableView data. 
        Once it is finished finishtableView by calling particular funciton
        """
        self.setWindowTitle("Neural network project")
        self.ui.prevBtn.clicked.connect(self.goBackToFileChooser)
        self.ui.prevBtn.setEnabled(True)
        self.ui.nextBtn.setEnabled(True)
        self.connect(self.paralelFiller, QtCore.SIGNAL("fillerDone()"),self.finishTableView)
        self.connect(self.paralelFiller, QtCore.SIGNAL("updateProgressBar()"), self.raiseProgressBar)
        self.ui.nextBtn.clicked.connect(self.goForwardToLearning)

    def raiseProgressBar(self):
        """
        Simple convinience funciton to raise Progressbar value by one when told to
        """
        progresVal = self.ui.progressBar.value()
        self.ui.progressBar.setValue(progresVal +1)
            
         
        
    def finishTableView(self):
        """
        Finish table view is a funciton that is called after nonGUI thread has finished all its work and data is ready to be displayed
        """
        class SampleTableModel(QtCore.QAbstractTableModel):
            """
            Inner class of the function that is used as model of the tableView
            """
            def __init__(self, datain, headerdata, parent=None, *args): 
                QtCore.QAbstractTableModel.__init__(self, parent, *args) 
                self.arraydata = datain
                self.headerdata = headerdata
 
            def rowCount(self, parent): 
                return len(self.arraydata) 
         
            def columnCount(self, parent): 
                return len(self.arraydata[0]) 
         
            def data(self, index, role): 
                if not index.isValid(): 
                    return QtCore.QVariant() 
                elif role != QtCore.Qt.DisplayRole: 
                    return QtCore.QVariant() 
                return QtCore.QVariant(self.arraydata[index.row()][index.column()]) 
        
            def headerData(self, col, orientation, role):
                if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
                    return QtCore.QVariant(self.headerdata[col])
                return QtCore.QVariant()
        
            def sort(self, Ncol, order):
                """Sort table by given column number.
                """
                self.emit(QtCore.SIGNAL("layoutAboutToBeChanged()"))
                self.arraydata = sorted(self.arraydata, key=operator.itemgetter(Ncol))        
                if order == QtCore.Qt.DescendingOrder:
                    self.arraydata.reverse()
                self.emit(QtCore.SIGNAL("layoutChanged()"))
        tableHeader = [_fromUtf8('Den'), _fromUtf8('Měsíc'), _fromUtf8('Rok'), _fromUtf8('Cena vstupenky'), _fromUtf8('Počet diváků'), _fromUtf8('Hodnocení')]
        tableModel = SampleTableModel(self.tabledata,tableHeader,self)
        self.ui.tableView.setModel(tableModel)
        self.ui.tableView.setShowGrid(False)
        vh = self.ui.tableView.verticalHeader()
        vh.setVisible(False)
        hh = self.ui.tableView.horizontalHeader()
        hh.setStretchLastSection(True)
        self.ui.tableView.resizeColumnsToContents()
        self.ui.numberOfSamples.setText(str(len(self.tabledata)) + _fromUtf8(" Vzorků"))
        
    
                    
            
        
        
    
    
    def goBackToFileChooser(self):
        """
        Function that is called if user clicks back button from samples window
        """

        self.ui= Ui_NeuralPredictor()
        self.ui.setupUi(self)
        self.connectSingalsWindow1()
        self.ui.nextBtn.setEnabled(True)
        self.ui.progressBar.setValue(99)
        self.ui.textPath.setText(self.main.sourceFile)
        self.show()
    
    def goForwardToLearning(self):
        """
        The actual window and its widgets dont change for learning. Just the proper signals get connected
        """
        self.connectSignalsToWindow3()
        
    
    
    def connectSignalsToWindow3(self):
        """
        Connect singals for third window (just modify window2)
        """
        try:
            self.ui.prevBtn.clicked.disconnect(self.goBackToFileChooser)
        except TypeError, e:
            pass
        self.ui.prevBtn.clicked.connect(self.goBackTosamples)
        self.ui.sipka2.setEnabled(True)
        self.ui.Uceni.setEnabled(True)
        self.animateLearning()
        self.connect(self.animThread, QtCore.SIGNAL("anim1()"), self.anim1)
        self.connect(self.animThread, QtCore.SIGNAL("anim2()"), self.anim2)
        self.connect(self.animThread, QtCore.SIGNAL("anim3()"), self.anim3)
        self.connect(self.animThread, QtCore.SIGNAL("anim4()"), self.anim4)
        self.animThread.start()
        self.trainNetwork()
    
    def trainNetwork(self):
        try:
            if self.main.net:
                pass
        except AttributeError,e :
            self.main.net = Network(self.main.dataSet)
            self.main.net.start()
    def goBackTosamples(self):
        self.ui.NahledLabel.setText(_fromUtf8("Náhled"))
        self.animThread.stop()
        self.ui.sipka2.setEnabled(False)
        self.ui.Uceni.setEnabled(False)
        self.ui.prevBtn.clicked.disconnect(self.goBackTosamples)
        self.ui.prevBtn.clicked.connect(self.goBackToFileChooser)
        self.ui.nextBtn.clicked.connect(self.connectSignalsToWindow3)
        
        
    
    def anim1(self):
        """
        Animate learning label step1
        """       
        self.ui.NahledLabel.setText(_fromUtf8("Učím se vzorky"))
    def anim2(self):
        """
        Animate learning label step2
        """
        self.ui.NahledLabel.setText(_fromUtf8("Učím se vzorky."))
    def anim3(self):
        """
        Animate learning label step3
        """
        self.ui.NahledLabel.setText(_fromUtf8("Učím se vzorky.."))
    def anim4(self):
        """
        Animate learning label step4
        """
        self.ui.NahledLabel.setText(_fromUtf8("Učím se vzorky..."))
    def animateLearning(self):
        """
        Function that starts a non-gui thread that emits signals that gui thread reacts to by animating
        """
        class MyAnimThread(QtCore.QThread):
            """
            Inner class of the function to run side-by-side with thread and create animation
            """
            def __init__(self):
                QtCore.QThread.__init__(self)
                self.halt = False
            def stop(self):
                self.halt = True
            def animate(self, animNumber):
                if not (self.halt):
                    self.emit(QtCore.SIGNAL("anim"+animNumber+"()"))
                    
               
            def run(self):
                """
                Run the thread,sleep it and change signal to be emited peridically
                """
                while True:
                    if (self.halt):
                        self.emit(QtCore.SIGNAL("animationDone()"))
                        break
                    else:
                        self.animate("1")
                        self.msleep(150)
                        if not self.halt:
                            self.animate("2")
                        self.msleep(150)
                        if not self.halt:
                            self.animate("3")
                        self.msleep(150)
                        if not self.halt:
                            self.animate("4")
                        self.msleep(150)
                    
        self.animThread = MyAnimThread()
            
        
        
    