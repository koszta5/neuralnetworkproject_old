'''
Created on Feb 13, 2012

@author: Pavel Kostelnik
'''
from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
import numpy 
from PyQt4 import QtCore
class Network(QtCore.QThread):
    '''
    classdocs
    '''
    def run(self):
        self.stardartizeDataSet()
        self.train()

    def stardartizeDataSet(self):
        for sample in self.dataSetObject.samples:
            self.dataSet.addSample((sample.day, sample.month, sample.year, sample.price, sample.star), (sample.audition,))
        
        
    def train(self):
        self.trainer = BackpropTrainer(self.net, self.dataSet)
        self.trainer.trainEpochs(5)
        print "trained"
        
    def __init__(self, data):
        '''
        Constructor
        '''
        QtCore.QThread.__init__(self)
        self.dataSetObject = data
        self.net = buildNetwork(5,14,1, bias=True)
        self.net.activate([numpy.random.rand(), numpy.random.rand(), numpy.random.rand(), numpy.random.rand(), numpy.random.rand()])
        self.dataSet = SupervisedDataSet(5,1)
        