'''
Created on Feb 13, 2012

@author: Pavel Kostelnik
'''
from threading import Thread
import re
from dataSample import DataSample
class ParalelSampleConstructor(Thread):
    '''
    This class implements threaded approach to loading the data from excell file into dataSetProvider object
    '''


    def __init__(self,dataSetProvider,date, audition, price, star):
        '''
        Constructor
        @param dataSetProvider: provider where to store the parsed data
        @param date: the date string to be parsed
        @param audition: audition to be stored
        @param price: price to be stored in the dataSample object
        '''
        Thread.__init__(self)
        self.dataSetProvider = dataSetProvider
        self.date = date
        self.audition = audition
        self.price = price
        self.star = star
        
    def run(self):
        """
        generate DataSample by parsing the stored values of this Thread object and add the DataSample to list of the dataSetProvider instance
        """
        if re.match(r"\d{4}-\d{2}-\d{2}", self.date):
            array_date = self.date.split("-")
            year = array_date[0]
            month = array_date[1]
            day = array_date[2]
            self.dataSetProvider.addDataSample(DataSample(day=day, month=month, year=year, price=self.price, audition= self.audition, star=self.star))
            