'''
Created on Feb 22, 2012

@author: Pavel Kostelnik
'''
from PyQt4 import QtCore
class ParalelTableFiller(QtCore.QThread):
    '''
    This thread is responsible for filling up the tableView with data from datasamples class
    '''


    def __init__(self,gui):
        '''
        Constructor
        '''
        QtCore.QThread.__init__(self)
        self.gui = gui


    def run(self):
        self.fillTableView()

    
    def fillTableView(self):
        """
        fill the table view with data and emit signals to change the gui progressbar. Emits fillerDone() signal after the thread is finished.
        This signal triggers finishtableView function of gui thread
        """
        
        self.tabledata = []
        raiseProgressBar = len(self.gui.main.dataSet.samples) / 100
        i = 1
        for sample in self.gui.main.dataSet.samples:
            sample_list = []
            sample_list.append(sample.day)
            sample_list.append(sample.month)
            sample_list.append(sample.year)
            sample_list.append(sample.price)
            sample_list.append(sample.audition)
            sample_list.append(sample.star)
            self.tabledata.append(sample_list)
            if (i < raiseProgressBar):
                i += 1
            else:
                i = 1
                self.emit(QtCore.SIGNAL("updateProgressBar()"))
            
        self.gui.tabledata = self.tabledata
        self.emit(QtCore.SIGNAL("fillerDone()"))