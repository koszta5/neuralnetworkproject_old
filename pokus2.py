from PyQt4 import QtGui
from PyQt4.QtCore import Qt
import time
import sys

class TestWindow(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        l = QtGui.QVBoxLayout()
        slider = QtGui.QSlider(Qt.Horizontal, self)
        slider.setRange(0,100)
        l.addWidget(slider)
        self.pb = QtGui.QProgressBar(self)
        self.pb.setRange(0,20)
        l.addWidget(self.pb)
        self.setLayout(l)
        slider.valueChanged.connect(self.doStuff)

    def doStuff(self, v):
        for i in xrange(20):
            self.pb.setValue(i)
            self.pb.repaint()
            time.sleep(0.05)
        self.pb.reset()

app = QtGui.QApplication(sys.argv)
w = TestWindow()
w.show()
app.exec_()
